<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Models\User;

// TODO : Rest + json: Mostrar los parámetros que recibes en un json
// TODO : Crear rama para este proyecto y subirlo.
// TODO : Ejecutar test sobre estas rutas


$router->get('/', function () use ($router) {
    $all_users = User::all();
    return $all_users;
});

$router->get('/1', function () use ($router) {
    $reg1 = User::find(1);
    return $reg1;
});

$router->get('/version', function () use ($router) {
    return $router->app->version();
});

// info: Info variables de entorno
$router->get('/info', function () use ($router) {
    var_dump( $_REQUEST );
    var_dump( $_SERVER );
    return "-- OK --";
});

// catch_exception: 
// Provocar una excepción devolvería una render DEFAULT con excepción 404 
// si no encuentra user id, si existe responde con 200. Ahora siempre devuelve 200 porque controlamos la excepción
$router->get('/catch_exception', function () use ($router) {
    // En App/Exceptions/Handler, subrutina de casos como este gestiónan la respuesta.
    return User::findOrFail(999999);
});

// create-new-sample-reg
// NOTA: obliga a tener campos "created_at" y "update_at".
$router->get('/create-new-sample-reg', function () use ($router) {
    return User::create(['name' => 'Jone', 'surname' => 'Doe', 'email' => 'john@doe.test']);
});

// read-and-uodate-user
$router->get('/read-and-update-user', function () use ($router) {
    $user = User::findOrFail(1);
    $user->name = 'create-new';
    $user->surname = 'sample-reg';
    $user->email = 'sample@reg.com';
    $user->save();
    return "Leido y actualizado usuario id 1 a name: create-new / surname sample-re";
});
